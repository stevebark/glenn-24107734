import sys
import os
import unittest
import json

from jobs import process_foo, process_bar
from processor import process_json_message

class TestJobsFixture(unittest.TestCase):
    
    def test_foo(self):
        result = process_foo(123)
        self.assertTrue('quote' in result)
        
    def test_bar(self):
        result = process_bar(234)
        self.assertTrue('bar' in result)
        
    def test_bar_failure(self):
        try:
            process_bar(123)
            self.assertTrue(False, 'process_bar should have raised an exception for odd-numbered job_id')
        except Exception:
            pass
        
    def test_json_invokation(self):
        payload = {
            'job': 'test',
            'id': 123
        }
        
        called_count = 0
        
        def fake_invokation(job_type, job_id):
            nonlocal called_count
            called_count += 1
            self.assertTrue(job_type, 'test')
            self.assertTrue(job_id, 123)
            return {
                'foo': 'bar'
            }
        
        json_result = process_json_message(json.dumps(payload), fake_invokation)
        result = json.loads(json_result)
        
        self.assertEqual(result['request'], payload)
        self.assertEqual(result.get('error'), None)
        self.assertEqual(result['result']['foo'], 'bar')
        self.assertTrue(called_count, 1)

if __name__ == '__main__':
    unittest.main()